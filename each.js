function each(items,callback){
    if(!Array.isArray(items)){
        return "Items is not an Array";
    }
    
    for(let index = 0; index < items.length; index++){
        callback(items,index)
    }
}

module.exports = each;
