const findProblem = require('../find');
const items = [1,2,3,4,5,5];

function callback(item){
    return item % 2 === 0;
}

const result = findProblem(items,callback);
console.log(result)
