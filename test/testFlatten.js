const flattenProblem = require('../flatten');
const nestedArray = [1, [2], [[3]], [[[4]]]];

const result = flattenProblem(nestedArray);
console.log(result);
