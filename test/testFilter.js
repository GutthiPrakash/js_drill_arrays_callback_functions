const filterProlem = require('../filter');
const items = [1,2,3,4,5,5];

function callback(element){
    return element % 2 === 0;
}

const result = filterProlem(items,callback);
console.log(result);
 