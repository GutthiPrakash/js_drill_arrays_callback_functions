function reduce(items,callback,startingValue){
    if(!Array.isArray(items)){
        return "Items is not an Array";
    }
    
    for(let index = 0; index < items.length; index++){
        startingValue = callback(startingValue,items[index]);
    }
    return startingValue;

}

module.exports = reduce;