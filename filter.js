function filter(items,callback){
    if(!Array.isArray(items)){
        return "Items is not an Array";
    }
    
    let resArr = []
    for(let index = 0; index < items.length; index++){
        if(callback(items[index])){
            resArr.push(items[index])
        }
    }
    return resArr;
}

module.exports = filter;