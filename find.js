function find(items,callback){
    if(!Array.isArray(items)){
        return "Items is not an Array";
    }
    
    for(let index = 0; index < items.length; index ++){
        if(callback(items[index])){
            return items[index];
        }
    }
    return undefined;
}

module.exports = find;