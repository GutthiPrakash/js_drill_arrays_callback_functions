let flattenedArray = []
function flatten(nestedArray){
    if(!Array.isArray(nestedArray)){
        return "Items is not an Array";
    }
    
    for(let index = 0; index < nestedArray.length; index ++){
        if(Array.isArray(nestedArray[index])){
            flatten(nestedArray[index])
        }
        else{
            flattenedArray.push(nestedArray[index]);
        }
    }
    return flattenedArray;
}

module.exports = flatten;