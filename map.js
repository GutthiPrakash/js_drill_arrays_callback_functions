function map(items,callback){
    if(!Array.isArray(items)){
        return "Items is not an Array";
    }
    
    let resArray = [];
    for(let index = 0; index < items.length; index ++){
        resArray.push(callback(items,index));
    }
    return resArray;
}

module.exports = map;